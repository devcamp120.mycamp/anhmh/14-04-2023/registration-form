import { Col, Container, Row, FormGroup, Label, Input, Button } from "reactstrap"
import 'bootstrap/dist/css/bootstrap.min.css'

function RegistrationForm() {
    return (
        <>
            <Container>
                <Row>
                    <Col sm={12} className="text-center">
                        <h4>REGISTRATION FORM</h4>
                    </Col>
                </Row>
            </Container>
            <Container>
                <FormGroup row>
                    <Label
                        for="exampleFirstname"
                        sm={2}
                    >
                        Firstname
                        <span class="text-danger">(*)</span>
                    </Label>
                    <Col sm={4}>
                        <Input
                            id="exampleFirstname"
                            name="firstname"
                            placeholder="with a placeholder"
                            type="firstname"
                        />
                    </Col>
                    <Label
                        for="examplePassport"
                        sm={2}
                    >
                        Passport
                        <span class="text-danger">(*)</span>
                    </Label>
                    <Col sm={4}>
                        <Input
                            id="examplePassport"
                            name="passport"
                            placeholder="with a placeholder"
                            type="passport"
                        />
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Label
                        for="exampleLastname"
                        sm={2}
                    >
                        Lastname
                        <span class="text-danger">(*)</span>
                    </Label>
                    <Col sm={4}>
                        <Input
                            id="exampleLastname"
                            name="lastname"
                            placeholder="with a placeholder"
                            type="lastname"
                        />
                    </Col>
                    <Label
                        for="exampleEmail"
                        sm={2}
                    >
                        Email
                    </Label>
                    <Col sm={4}>
                        <Input
                            id="examplePassport"
                            name="email"
                            placeholder="with a placeholder"
                            type="email"
                        />
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Label
                        for="exampleBirthday"
                        sm={2}
                    >
                        Birthday
                        <span class="text-danger">(*)</span>
                    </Label>
                    <Col sm={4}>
                        <Input
                            id="exampleBirthday"
                            name="birthday"
                            placeholder="with a placeholder"
                            type="birthday"
                        />
                    </Col>
                    <Label
                        for="exampleCountry"
                        sm={2}
                    >
                        Country
                        <span class="text-danger">(*)</span>
                    </Label>
                    <Col sm={4}>
                        <Input
                            id="exampleCountry"
                            name="country"
                            placeholder="with a placeholder"
                            type="country"
                        />
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Label
                        for="exampleGender"
                        sm={2}
                    >
                        Gender
                        <span class="text-danger">(*)</span>
                    </Label>
                    <Col sm={4}>
                        <FormGroup check inline>
                            <Label check>
                                Male
                            </Label>
                            <Input
                                name="Male"
                                type="radio"
                            />{' '}
                        </FormGroup>
                        <FormGroup check inline>
                            <Label check>
                                Female
                            </Label>
                            <Input
                                name="Female"
                                type="radio"
                            />{' '}
                        </FormGroup>
                    </Col>
                    <Label
                        for="exampleRegion"
                        sm={2}
                    >
                        Region
                    </Label>
                    <Col sm={4}>
                        <Input
                            id="exampleRegion"
                            name="region"
                            placeholder="with a placeholder"
                            type="region"
                        />
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Label
                        for="exampleSubject"
                        sm={2}
                    >
                        Subject
                    </Label>
                    <Col sm={10}>
                        <Input
                            id="exampleSubject"
                            name="subject"
                            type="textarea"
                        />
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Col className="button-align">
                        <Button color="success" className="check-data-btn">Check Data</Button>{' '}
                        <Button color="success">Send</Button>
                    </Col>

                </FormGroup>
            </Container>

        </>
    )
}
export default RegistrationForm